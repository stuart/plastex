Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: plastex
Upstream-Contact: Kevin D. Smith <Kevin.Smith@sas.com>
Source: https://github.com/tiarno/plastex

Files: *
Copyright: 2007-2014 Kevin D. Smith <Kevin.Smith@sas.com>
License: Expat

Files: plasTeX/Renderers/PageTemplate/simpletal/*
Copyright: 2004-2005 Colin Stewart (http://www.owlfish.com/)
License: SimpleTAL

Files: plasTeX/Base/LaTeX/allkeys.txt
Copyright: 2001-2006 Unicode, Inc.
License: UNICODE

Files: plasTeX/Renderers/S5/*
Copyright: 1995-2007 Eric A. Meyer
License: S5

Files: plasTeX/Base/LaTeX/ent.xml
Copyright: 2000,2007 Rune Mathisen, Vidar Bronken Gundersen. http://www.bitjungle.com/isoent
License: BSD-3-clause

Files: plasTeX/Base/LaTeX/pyuca.py
Copyright: 2006 James Tauber
License: Expat

Files: debian/*
Copyright: 2007-2009 Carl Fürstenberg <azatoth@gmail.com>
License: GPL-2+
 Packaging is licensed under the GPL, version 2 or later,
 see `/usr/share/common-licenses/GPL'.

License: BSD-3-clause
        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
 .
          * Redistributions of source code must retain the above copyright
            notice, this list of conditions and the following disclaimer.
          * Redistributions in binary form must reproduce the above copyright
            notice, this list of conditions and the following disclaimer in the
            documentation and/or other materials provided with the distribution.
          * Neither the name of the <organization> nor the
            names of its contributors may be used to endorse or promote products
            derived from this software without specific prior written permission.
 .
        THIS SOFTWARE IS PROVIDED BY <copyright holder> ``AS IS'' AND ANY
        EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL <copyright holder> BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
 .
    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.
 .
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.

License: S5
        From http://meyerweb.com/eric/tools/s5/faq.html:
        "What are the licensing terms, and how do they affect the content of a slide
        show done in S5?
 .
        First, whatever license S5 has or did have, it will only ever apply to the
        technology, not the content. You can prepare a confidential presentation 
        where the content is not only copyrighted and patented, but Top Secret as 
        well. S5's license will not affect it.
 .
        As of version 1.1, the technology itself has been explicitly released 
        into the Public Domain, so there are no restrictions whatsoever on its use 
        or reuse (nor can there ever be). S5 v1.0 was published under a 
        Creative Commons Attribution-ShareAlike 2.0 license, which still holds for
        that version of the software. The change was made because I found out CC
        licenses aren't appropriate for software only after 1.0 came out. Oops."

License: SimpleTAL
        SimpleTAL 4.1
        --------------------------------------------------------------------
        Copyright (c) 2005 Colin Stewart (http://www.owlfish.com/)
        All rights reserved.
 .
        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions
        are met:
        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.
        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.
        3. The name of the author may not be used to endorse or promote products
           derived from this software without specific prior written permission.
 .
        THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
        IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
        OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
        IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
        INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
        NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
        DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
        THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
        THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: UNICODE
        UNICODE, INC. LICENSE AGREEMENT - DATA FILES AND SOFTWARE
 .
        Unicode Data Files include all data files under the directories 
        http://www.unicode.org/Public/, http://www.unicode.org/reports/, and 
        http://www.unicode.org/cldr/data/ . Unicode Software includes any source 
        code published in the Unicode Standard or under the directories 
        http://www.unicode.org/Public/, http://www.unicode.org/reports/, and 
        http://www.unicode.org/cldr/data/.
 .
        NOTICE TO USER: Carefully read the following legal agreement. BY 
        DOWNLOADING, INSTALLING, COPYING OR OTHERWISE USING UNICODE INC.'S DATA 
        FILES ("DATA FILES"), AND/OR SOFTWARE ("SOFTWARE"), YOU UNEQUIVOCALLY 
        ACCEPT, AND AGREE TO BE BOUND BY, ALL OF THE TERMS AND CONDITIONS OF THIS 
        AGREEMENT. IF YOU DO NOT AGREE, DO NOT DOWNLOAD, INSTALL, COPY, DISTRIBUTE 
        OR USE THE DATA FILES OR SOFTWARE.
 .
        COPYRIGHT AND PERMISSION NOTICE
 .
        Copyright © 1991-2007 Unicode, Inc. All rights reserved. Distributed under 
        the Terms of Use in http://www.unicode.org/copyright.html.
 .
        Permission is hereby granted, free of charge, to any person obtaining a copy 
        of the Unicode data files and any associated documentation (the "Data Files") 
        or Unicode software and any associated documentation (the "Software") to 
        deal in the Data Files or Software without restriction, including without 
        limitation the rights to use, copy, modify, merge, publish, distribute, 
        and/or sell copies of the Data Files or Software, and to permit persons to 
        whom the Data Files or Software are furnished to do so, provided that (a) 
        the above copyright notice(s) and this permission notice appear with all 
        copies of the Data Files or Software, (b) both the above copyright notice(s) 
        and this permission notice appear in associated documentation, and (c) there 
        is clear notice in each modified Data File or in the Software as well as in 
        the documentation associated with the Data File(s) or Software that the data 
        or software has been modified.
 .
        THE DATA FILES AND SOFTWARE ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY 
        KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
        MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF 
        THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS 
        INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT 
        OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF 
        USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER 
        TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE 
        OF THE DATA FILES OR SOFTWARE.
 .
        Except as contained in this notice, the name of a copyright holder shall not 
        be used in advertising or otherwise to promote the sale, use or other 
        dealings in these Data Files or Software without prior written authorization 
        of the copyright holder.
 .
        Unicode and the Unicode logo are trademarks of Unicode, Inc., and may be 
        registered in some jurisdictions. All other trademarks and registered 
        trademarks mentioned herein are the property of their respective owners.
